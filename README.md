# README #


### What is this repository for? ###

This is a sample iOS app to learn how to use *WallboxSDK* pod.
In this sample project you will find how to use some functionalities in 3 different views:


#### 1. ChargersViewController ####

This is the view you will find by default when you run the project on your device. 

In this view, you can find an example of use for:

* [Login](https://wallbox.bitbucket.io/ios/Protocols/WallboxUserService.html)
* Obtain the [list of chargers](https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html) for the current user
* Start/Stop [scanning chargers](https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html) in reach of bluetooth
* Check if a specific charger is currently [in range](https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html) of the bluetooth signal
* Obtain the instance of [WallboxCharger](https://wallbox.bitbucket.io/ios/Charger.html) neeced to operate with the charger.


#### 2. AddChargerViewController ####

This view is accessible from the *plus* button on ChargersViewController's navigation bar. 

In this view, you can find an example of use for:

* [Link a charger](https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html) to the current user


#### 3. DetailsViewController ####

To access this view, press one of the chargers listed in ChargersViewController. 

In this view, you can find an example of use for:

* [Connect/Disconnect the charger](https://wallbox.bitbucket.io/ios/Protocols/ConnectFeature.html)
* [Pair the charger](https://wallbox.bitbucket.io/ios/Protocols/PairFeature.html) 
* [Subscribe/Unsubscribe](https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html) to the changes of the charger [Status](https://wallbox.bitbucket.io/ios/Structs/WallboxChargerStatus.html) 
* Keep the current [status](https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html) up to date in real time
* [Start/Stop](https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html) status notifier
* Handling when a charger is connected or disconnected
* Recommended actions when going background and foreground
* [Start/stop charging](https://wallbox.bitbucket.io/ios/Protocols/ChargingFeature.html)
* [Lock/unlock charger](https://wallbox.bitbucket.io/ios/Protocols/LockFeature.html)
* [Set/Edit name of charger](https://wallbox.bitbucket.io/ios/Protocols/SetNameFeature.html)
* [Unlink charger](https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html) from the current user


### How do I get set up? ###

* This project uses [Cocoapods](https://guides.cocoapods.org/using/getting-started.html) as a dependency manager. To set it up, run *pod install* in terminal.

* Search for *Credentials.txt* and copy paste your provided token. You will find the file in the following path:
`WallboxSDK-Sample/Common/Credentials.txt`

* You are ready to go!


### How do I make WallboxSDK work on my own app? ###

First of all, ensure you added [Cocoapods](https://guides.cocoapods.org/using/getting-started.html) to your project and install *WallboxSDK* pod. 

You will need a token to [login](https://wallbox.bitbucket.io/ios/Protocols/WallboxUserService.html) to WallboxSDK and use it's methods and properties. This token will be provided by the **Wallbox External API**'s service to create a user account on the Wallbox system: *ext/create*.

Once you import *WallboxSDK* on your own code, to start using it's services, some small actions are required to get your project ready:

* Add required message to tell the user why the app needs access to Bluetooth by adding the key [NSBluetoothAlwaysUsageDescription](https://developer.apple.com/documentation/bundleresources/information_property_list/nsbluetoothalwaysusagedescription) to Info.plist file.

* Add *App Groups* capability to the project. For more information about it, find apple's documentation to [Add a capability to a target](https://help.apple.com/xcode/mac/current/#/dev88ff319e7).

* At *didFinishLaunchingWithOptions* in the *AppDelegate*, instantiate the sdk by adding these 2 lines:

```sh
WallboxSDK.shared.testMode = true
WallboxSDK.shared.appGroup = "insert_your_appgroup_id_here"
```
