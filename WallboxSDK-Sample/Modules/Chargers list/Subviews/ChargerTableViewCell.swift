//
//  ChargerTableViewCell.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 19/1/21.
//

import UIKit

class ChargerTableViewCell: UITableViewCell {
    
    static let identifier = "ChargerTableViewCell"

    @IBOutlet weak var chargerImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    func populate(_ model: ChargerViewModel) {
        if let imageUrl = model.imageUrl {
            chargerImageView.kf.setImage(with: imageUrl)
        }
        nameLabel.text = model.name
        statusLabel.text = model.status.description
        statusLabel.textColor = model.status.color
    }
}
