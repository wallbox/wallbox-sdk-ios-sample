//
//  ChargersViewModel.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 18/1/21.
//

import Foundation
import WallboxSDK
import Kingfisher

struct ChargerStatus {
    let description: String
    let color: UIColor
}

struct ChargerViewModel {
    let charger: WallboxChargerInfo
    let bluetoothAvailability: Bool
    
    var identifier: Int {
        charger.id
    }
    
    var name: String {
        charger.name
    }
    
    var imageUrl: URL? {
        return charger.imageUrl
    }

    private var internetAvailability: Bool {
        return charger.state != .unavailable
    }
    
    private var internetConnectionType: WallboxInternetConnectionType? {
        return charger.internetConnectionType
    }
    
    var status: ChargerStatus {
        let connectionType = internetConnectionType?.name ?? "Online"
        var connection: String = ""
        
        switch (bluetoothAvailability, internetAvailability) {
        case (true, true):
            connection = "Available: Bluetooth, \(connectionType)"
        case (true, false):
            connection = "Available: Bluetooth"
        case (false, true):
            connection = "Available: \(connectionType)"
        case (false, false):
            connection = "Not Available: Offline"
        }
        
        return ChargerStatus(description: connection, color: (bluetoothAvailability || internetAvailability) ? UIColor.green : UIColor.red)
    }
}

class ChargersViewModel {
    
    var chargers: [ChargerViewModel]
    
    init(chargers: [ChargerViewModel]) {
        self.chargers = chargers
    }
    
    var numberOfChargers: Int {
        chargers.count
    }
    
    func updateBluetoothAvailabilityAndGetIndex(_ identifier: Int) -> Int? {
        guard let index = self.chargers.firstIndex(where: { $0.identifier == identifier }) else { return nil }
        chargers[index] = ChargerViewModel(charger: chargers[index].charger, bluetoothAvailability: true)
        return index
    }
}
