//
//  ChargersViewController.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 15/1/21.
//

import UIKit
import WallboxSDK

class ChargersViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    // Check available services in the documentation: https://wallbox.bitbucket.io/ios/Services.html
    private let service = WallboxSDK.serviceSupplier.chargerService

    private var viewModel: ChargersViewModel = ChargersViewModel(chargers: [])
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(chargersList), for: UIControl.Event.valueChanged)
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        chargersList()
    }
}

// MARK: - UITableViewDataSource and UITableViewDelegate
extension ChargersViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfChargers
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ChargerTableViewCell.identifier, for: indexPath) as? ChargerTableViewCell else {
            return UITableViewCell()
        }
        cell.populate(viewModel.chargers[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigateToDetail(serialNumber: viewModel.chargers[indexPath.row].identifier)
    }
}

// MARK: - Private
extension ChargersViewController {    
    // Obtain the list of chargers for the current user
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
    @objc private func chargersList() {
        service.getChargersInfo { [weak self] result in
            switch result {
            case .success(let chargers):
                self?.populateChargersList(chargers)
                self?.scanChargers()
            case .failure(let error):
                self?.showError("getChargersInfo method failed")
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
            @unknown default: break
            }
        }
    }
    
    // Scan chargers in reach of bluetooth
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
    private func scanChargers() {
        service.scanChargers { [weak self] (device) in
            self?.deviceFound(device)
        } onScanFinished: { _ in }
    }
    
    private func deviceFound(_ device: WallboxBluetoothDevice) {
        DispatchQueue.main.async { [weak self] in
            guard let index = self?.viewModel.updateBluetoothAvailabilityAndGetIndex(device.id) else { return }
            let indexPath = IndexPath(row:  index, section: 0)
            self?.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    private func viewModel(chargers: [WallboxChargerInfo]) -> ChargersViewModel {
        let myChargers = chargers.compactMap { chargerInfo -> ChargerViewModel? in
            // Check if a specific charger is currently in range of the bluetooth signal
            // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
            let inRange = self.service.isInRange(serialNumber: chargerInfo.id)
            
            return ChargerViewModel(charger: chargerInfo, bluetoothAvailability: inRange)
        }
        return ChargersViewModel(chargers: myChargers)
    }
    
    private func navigateToDetail(serialNumber: Int) {
        let viewController = DetailsViewController()
        
        // Obtain the instance of WallboxCharger needed to operate with the charger.
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
        // We need this object to use all the features enumerated in the documentation: https://wallbox.bitbucket.io/ios/Features.html
        guard let charger = service.createCharger(serialNumber: serialNumber) else {
            showError("createCharger method returned nil")
            return
        }
        
        viewController.charger = charger
        
        // Stop scanning chargers in reach of bluetooth
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
        service.stopScanChargers()
        
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func populateChargersList(_ chargers: [WallboxChargerInfo]) {
        self.viewModel = viewModel(chargers: chargers)
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
            self?.refreshControl.endRefreshing()
        }
    }
    
    @objc private func addChargerTapped() {
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.pushViewController(AddChargerViewController(), animated: true)
        }
    }
    
    private func setupTableView() {
        tableView.register(UINib(nibName: ChargerTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ChargerTableViewCell.identifier)
        tableView.rowHeight = 100.0
        tableView.delegate = self
        tableView.dataSource = self
        tableView.refreshControl = refreshControl
    }
    
    private func setupNavigationBar() {
        title = "WalboxSDK"
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addChargerTapped))
        navigationItem.hidesBackButton = true
    }
}
