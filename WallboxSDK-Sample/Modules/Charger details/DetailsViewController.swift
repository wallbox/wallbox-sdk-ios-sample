//
//  DetailsViewController.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 25/1/21.
//

import UIKit
import WallboxSDK

class DetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let cellIdentifier = "cellIdentifier"
    
    var charger: WallboxCharger? {
        didSet {
            self.detailsViewModel = DetailsViewModel(charger!)
        }
    }
    
    var detailsViewModel: DetailsViewModel!
    
    deinit {
        // Don't forget to unsubscribe when the subscribed target (in this case self) is deallocated
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html
        charger?.unsubscribe(self)
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    
        setTitle(beforeConnect: true)
        setupTableView()
        connectCharger()
        setupNotificationCenter()
    }
}

// MARK: - UITableViewDataSource
extension DetailsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return detailsViewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return detailsViewModel.titleForHeaderInSection(section)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        detailsViewModel.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellDetails = detailsViewModel.cellDetails(indexPath)
        
        var cell: UITableViewCell {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) else {
                return UITableViewCell(style: UITableViewCell.CellStyle.value1, reuseIdentifier: cellIdentifier)
            }
            return cell
        }
        
        switch cellDetails {
        case .info(let viewModel):
            let myCell = cell
            myCell.textLabel?.text = viewModel.title
            myCell.textLabel?.textColor = UIColor.black
            myCell.detailTextLabel?.text = viewModel.detail
            myCell.selectionStyle = .none
            return myCell
        case .action(let viewModel):
            let myCell = cell
            myCell.textLabel?.text = viewModel.text
            myCell.textLabel?.textColor = UIColor.systemBlue
            myCell.detailTextLabel?.text = ""
            myCell.selectionStyle = .default
            return myCell
        }
    }
}

// MARK: - UITableViewDelegate
extension DetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard indexPath.section == 1 else { return }
        let cellDetails = detailsViewModel.cellDetails(indexPath)
        
        switch cellDetails {
        case .action(let model):
            switch model.type {
            case .charge:
                chargeAction()
            case .lock:
                lockAction()
            case .editName:
                editNameAction()
            case .unlinkCharger:
                unlinkChargerAction()
            }
        default: break
        }
    }
}

// MARK: - Private
extension DetailsViewController {
    // 1. Connect the charger
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/ConnectFeature.html
    private func connectCharger() {
        charger?.connect(onConnect: { [weak self] result in
            self?.setTitle()
            switch result {
            case .success:
                self?.pairCharger()
            case .failure(let error):
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("connect method failed")
            @unknown default: break
            }
            
        }, onDisconnect: { [weak self] in
            print("onDisconnect")
            // Stop receiving the changes of the charger Status
            // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html
            self?.charger?.stopStatusNotifier() 
        })
    }
    
    // 2. Pair the charger
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/PairFeature.html
    private func pairCharger() {
        charger?.pair { [weak self] result in
            switch result {
            case .success:
                self?.subscribeForStatus()
            case .failure(let error):
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("pair method failed")
            @unknown default: break
            }
        }
    }
    
    // 3. Subscribe to the changes of the charger Status and keep it up to date
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html
    private func subscribeForStatus() {
        charger?.subscribe(self, onStatusChanged: { [weak self] status in
            self?.reloadData(status: status)
        })
        // 4. Start receiving the changes of the charger Status
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/StatusFeature.html
        charger?.startStatusNotifier()
    }
    
    private func reloadData(status: WallboxChargerStatus) {
        setTitle()
        DispatchQueue.main.async { [weak self] in
            self?.detailsViewModel.update(status)
            self?.tableView.reloadData()
        }
    }
    
    private func chargeAction() {
        // Check if charging feature is available for the current charger
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/ChargingFeature.html
        guard let charger = charger, charger.isChargingFeatureAvailable.isSuccess() else {
            self.showError("Charging feature not available")
            return
        }
        
        // Get the last known status of the charger
        // See documentation here: https://wallbox.bitbucket.io/ios/Classes/WallboxCharger.html
        if charger.lastKnownStatus?.state == .charging {
            stopCharging()
        } else {
            startCharging()
        }
    }
    
    // Start charging with the current charger
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/ChargingFeature.html
    private func startCharging() {
        charger?.startCharging(completion: { [weak self] result in
            if let error = result.error {
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("startCharging method failed")
            }
        })
    }
    
    // Stop charging with the current charger.
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/ChargingFeature.html
    private func stopCharging() {
        charger?.stopCharging { [weak self] result in
            if let error = result.error {
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("stopCharging method failed")
            }
        }
    }
    
    private func lockAction() {
        // Check if lock feature is available for the current charger
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/LockFeature.html
        guard let charger = charger, charger.isLockFeatureAvailable.isSuccess() else {
            self.showError("Lock feature not available")
            return
        }
        
        // Get the last known status of the charger
        // See documentation here: https://wallbox.bitbucket.io/ios/Classes/WallboxCharger.html
        if charger.lastKnownStatus?.isLocked ?? false {
            unlock()
        } else {
            lock()
        }
    }
    
    // Lock the charger
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/LockFeature.html
    private func lock() {
        charger?.lock { [weak self] result in
            if let error = result.error {
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("lock method failed")
            }
        }
    }
    
    // Unlock the charger
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/LockFeature.html
    private func unlock() {
        charger?.unlock { [weak self] result in
            if let error = result.error {
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("unlock method failed")
            }
        }
    }

    private func editNameAction() {
        // Check if the set name feature feature is available for the current charger
        // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/SetNameFeature.html
        guard let charger = charger, charger.isSetNameFeatureAvailable.isSuccess() else {
            self.showError("Set name feature not available")
            return
        }
        
        showAlertWithInputField(title: "Edit name", message: "Type new value") { [weak self] name in
            guard !name.isEmpty else {
                self?.showError("Type a valid name")
                return
            }
            self?.setName(name)
        }
    }
    
    // Set or edit the name of the charger
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/SetNameFeature.html
    private func setName(_ name: String) {
        charger?.setName(name, completion: { [weak self] result in
            switch result {
            case .success:
                self?.setTitle()
            case .failure(let error):
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("setName method failed")
            default: break
            }
        })
    }

    // Unlink charger from the current user
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
    private func unlinkChargerAction() {
        guard let serialNumber = charger?.id else { return }
        let service = WallboxSDK.serviceSupplier.chargerService
        service.unlinkCharger(serialNumber: serialNumber) { [weak self] result in
            switch result {
            case .success:
                print("unlinked charger")
                self?.navigationController?.popViewController(animated: true)
            case .failure(let error):
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.showError("unlinkCharger method failed")
            @unknown default: break
            }
        }
    }
    
    private func setTitle(beforeConnect: Bool = false) {
        DispatchQueue.main.async { [weak self] in
            self?.navigationItem.titleView = self?.detailsViewModel.title(beforeConnect: beforeConnect)
        }
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    private func setupNotificationCenter() {
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: OperationQueue.current) { [weak self] _ in
            // Disconnect the charger
            // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/ConnectFeature.html
            self?.charger?.disconnect()
        }
        
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: OperationQueue.current) { [weak self] _ in
            self?.connectCharger()
        }
    }
}
