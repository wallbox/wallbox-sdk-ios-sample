//
//  ChargerActionViewModel.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 26/1/21.
//

import UIKit
import WallboxSDK

enum ActionType {
    case charge(_ text: String)
    case lock(_ text: String)
    case editName(_ text: String)
    case unlinkCharger(_ text: String)
}

struct ChargerActionViewModel {
    let type: ActionType
    
    var text: String {
        switch type {
        case .charge(let text), .lock(let text), .editName(let text), .unlinkCharger(let text):
            return text
        }
    }
    
    static func createChargeAction(_ status: WallboxChargerStatus?) -> ChargerActionViewModel? {
        guard let state = status?.state else {
            return nil
        }
        return ChargerActionViewModel(type: .charge(state == .some(WallboxChargerState.charging) ? "Pause" : "Play"))
    }
    
    static func createLockAction(_ status: WallboxChargerStatus?) -> ChargerActionViewModel? {
        guard let isLocked = status?.isLocked else {
            return nil
        }
        return ChargerActionViewModel(type: .lock(isLocked == true ? "Unlock" : "Lock"))
    }
    
    static func createEditNameAction() -> ChargerActionViewModel {
        ChargerActionViewModel(type: .editName("Edit charger name"))
    }
    
    static func createUnlinkChargerAction() -> ChargerActionViewModel {
        ChargerActionViewModel(type: .unlinkCharger("Unlink charger"))
    }
}
