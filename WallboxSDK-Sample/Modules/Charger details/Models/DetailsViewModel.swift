//
//  DetailsViewModel.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 25/1/21.
//

import UIKit
import WallboxSDK

enum DetailsType {
    case info(ChargerInfoViewModel)
    case action(ChargerActionViewModel)
}

enum StatusTitle: String {
    case connecting = "Connecting..."
    case notConnected = "Not connected"
    case connected = "Connected"
    
    var color: UIColor {
        switch self {
        case .notConnected:
            return UIColor.red
        default:
            return UIColor.green
        }
    }
}

class DetailsViewModel {
    var infoArray: [ChargerInfoViewModel] = []
    var actionsArray: [ChargerActionViewModel] = []
    let charger: WallboxCharger
    
    init(_ charger: WallboxCharger) {
        self.charger = charger
        update()
    }
    
    func update(_ status: WallboxChargerStatus? = nil) {
        
        self.infoArray = [
            ChargerInfoViewModel.connectionDetail(charger),
            ChargerInfoViewModel.version(charger),
            ChargerInfoViewModel.state(status),
            ChargerInfoViewModel.lockStatus(status)
        ]
        
        self.actionsArray.removeAll()
        
        if let chargeAction = ChargerActionViewModel.createChargeAction(status) {
            self.actionsArray.append(chargeAction)
        }
        if let lockAction = ChargerActionViewModel.createLockAction(status) {
            self.actionsArray.append(lockAction)
        }
        self.actionsArray.append(ChargerActionViewModel.createEditNameAction())
        self.actionsArray.append(ChargerActionViewModel.createUnlinkChargerAction())
    }
    
    func title(beforeConnect: Bool = false) -> UIView {
        var status: StatusTitle = .connecting
        if !beforeConnect {
            status = charger.isConnected ?.connected : .notConnected
        }
    
        let labelName = UILabel(frame: .zero)
        labelName.text = charger.name
        labelName.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
        let labelStatus = UILabel(frame: .zero)
        labelStatus.text = status.rawValue
        labelStatus.font = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        labelStatus.textColor = status == .notConnected ? .red : .green
        
        let stack = UIStackView(arrangedSubviews: [labelName, labelStatus])
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .equalCentering
        stack.spacing = 4.0
        
        return stack
    }
    
    var numberOfSections: Int {
        return 2
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        switch section {
        case 0:
            return infoArray.count
        case 1:
            return actionsArray.count
        default:
            return 0
        }
    }
    
    func titleForHeaderInSection(_ section: Int) -> String? {
        section == 1 ? "Actions" : nil
    }
    
    func cellDetails(_ indexPath: IndexPath) -> DetailsType {
        switch indexPath.section {
        case 0:
            return DetailsType.info(infoArray[indexPath.row])
        default:
            return DetailsType.action(actionsArray[indexPath.row])
        }
    }
}
