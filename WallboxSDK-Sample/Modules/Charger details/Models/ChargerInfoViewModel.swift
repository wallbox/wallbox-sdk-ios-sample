//
//  ChargerInfoViewModel.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 26/1/21.
//

import UIKit
import WallboxSDK

struct ChargerInfoViewModel {
    let title: String
    let detail: String
    
    static func connectionDetail(_ charger: WallboxCharger) -> ChargerInfoViewModel {
        var connection: String = "none"
        switch charger.connectedBy {
        case .bluetooth:
            connection = "bluetooth"
        case .cloud:
            connection = "cloud"
        default: break
        }
        return ChargerInfoViewModel(title: "Connection:", detail: connection)
    }
    
    static func version(_ charger: WallboxCharger) -> ChargerInfoViewModel {
        if charger.hasSoftware {
            return ChargerInfoViewModel(title: "Sofware version:", detail: charger.versions.softwareVersion)
        } else {
            return ChargerInfoViewModel(title: "Firmare version:", detail: "\(charger.versions.firmwareVersion)")
        }
    }
    
    static func state(_ status: WallboxChargerStatus?) -> ChargerInfoViewModel {
        guard let state = status?.state else {
            return ChargerInfoViewModel(title: "State:", detail: "...")
        }
        
        var text: String = ""
        switch state {
        case .available, .connectedSafetyMarginExceeded, .connectedWaitAdminAuthorization, .powerSharingWaitingCurrentAssignation, .connectedInQueuePowerBoost, .endSchedule:
            text = "Connected"
        case .unavailable, .ocppChargeNotAllowed:
            text = "Unavailable"
        case .charging, .ocppChargeFinishing:
            text = "Charging"
        case .discharging:
            text = "Discharging"
        case .connectedCarDemand:
            text = "Connected and waiting for to car demand"
        case .waitingNextSchedule:
            text = "Scheduled"
        case .paused:
            text = "Charging paused"
        case .error, .powerSharingUnconfigured:
            text = "Error"
        @unknown default: break
        }
        return ChargerInfoViewModel(title: "State:", detail: text)
    }
    
    static func lockStatus(_ status: WallboxChargerStatus?) -> ChargerInfoViewModel {
        guard let isLocked = status?.isLocked else {
            return ChargerInfoViewModel(title: "Lock status:", detail: "...")
        }
        return ChargerInfoViewModel(title: "Lock status:", detail: isLocked ? "Locked" : "Unlocked")
    }
}
