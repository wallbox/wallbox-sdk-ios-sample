//
//  AddChargerViewController.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 20/1/21.
//

import UIKit
import WallboxSDK

class AddChargerViewController: UIViewController {

    @IBOutlet weak var pinTextField: TextFieldView!
    @IBOutlet weak var pukTextField: TextFieldView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        hideKeyboardWhenTappedAround()
    }
    
    @IBAction func addButtonTapped(_ sender: Any) {
        guard let serialNumberString = pinTextField.text,
              let serialNumber = Int(serialNumberString) else {
            handlePinError("Enter a valid numeric PIN")
            return
        }
        guard let puk = pukTextField.text,
              !puk.isEmpty else {
            handlePukError("Enter a valid PUK")
            return
        }
        linkCharger(serialNumber: serialNumber, puk: puk)
    }
}

// MARK: - Private
extension AddChargerViewController {
    // Link a new charger to the current user
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxChargerService.html
    private func linkCharger(serialNumber: Int, puk: String) {
        let service = WallboxSDK.serviceSupplier.chargerService
        service.linkCharger(serialNumber: serialNumber, puk: puk) { [weak self] result in
            switch result {
            case .success:
                print("linked charger")
                self?.navigationController?.popViewController(animated: true)
            case .failure(let error):
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
                self?.handleWallboxError(error)
            @unknown default: break
            }
        }
    }
    
    private func handlePinError(_ error: String) {
        DispatchQueue.main.async { [weak self] in
            self?.pinTextField.showError(error)
            self?.pukTextField.removeError()
        }
    }
    
    private func handlePukError(_ error: String) {
        DispatchQueue.main.async { [weak self] in
            self?.pukTextField.showError(error)
            self?.pinTextField.removeError()
        }
    }
    
    private func handleRemoveErrors() {
        DispatchQueue.main.async { [weak self] in
            self?.pinTextField.removeError()
            self?.pukTextField.removeError()
        }
    }
    
    private func handleWallboxError(_ error: WallboxError) {
        switch error {
        case .apiError(let apiError):
            switch apiError {
            case .invalidChargerPuk:
                handlePukError("Invalid PUK for this charger")
            case .chargerNotExists:
                handlePinError("This charger does not exist")
            case .chargerAlreadyLinked:
                handlePinError("This charger is already in use")
            default:
                handlePinError("An error occurred")
            }
        default:
            handlePinError("An error occurred")
        }
    }
    
    private func setupUI() {
        title = "Add Charger"
        pinTextField.populate(title: "PIN")
        pukTextField.populate(title: "UID or PUK")
    }
}
