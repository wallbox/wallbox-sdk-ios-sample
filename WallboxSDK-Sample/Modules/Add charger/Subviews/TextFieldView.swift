//
//  TextFieldView.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 20/1/21.
//

import UIKit

class TextFieldView: UIView {
    
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        configureView()
    }
    
    var text: String? {
        textField.text
    }
    
    func populate(title: String) {
        titleLabel.text = title
        errorLabel.isHidden = true
    }
    
    func showError(_ error: String) {
        errorLabel.text = error
        errorLabel.isHidden = false
    }
    
    func removeError() {
        errorLabel.text = nil
        errorLabel.isHidden = true
    }
}

extension TextFieldView {
    private func configureView() {
        Bundle.main.loadNibNamed("TextFieldView", owner: self, options: nil)
        view.fixInView(self)
    }
}
