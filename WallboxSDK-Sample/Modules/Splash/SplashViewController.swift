//
//  SplashViewController.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 1/2/21.
//

import Foundation

import UIKit
import WallboxSDK

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigation()
        loginSDK(completion: navigateToChargersList)
    }
}

// MARK: - Private

extension SplashViewController {
    // Login with WallboxUserService
    // See documentation here: https://wallbox.bitbucket.io/ios/Protocols/WallboxUserService.html
    private func loginSDK(completion: @escaping (() -> Void)) {
        guard let token = CredentialsHelper.token else { return }
        let userService = WallboxSDK.serviceSupplier.userService
        userService.login(token: token) { [weak self] result in
            switch result {
            case .success:
                completion()
            case .failure(let error):
                self?.showError("login method failed")
                print(error) // WallboxError details: https://wallbox.bitbucket.io/ios/Enums/WallboxError.html
            @unknown default: break
            }
        }
    }
    
    private func navigateToChargersList() {
        let viewController = ChargersViewController()
        DispatchQueue.main.async { [weak self] in
            self?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    private func setupNavigation() {
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
