//
//  UIView+Extension.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 20/1/21.
//

import UIKit

extension UIView {
    func fixInView(_ container: UIView!) -> Void {
        container.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        frame = container.bounds
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: container, attribute: .trailing, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1.0, constant: 0.0),
            NSLayoutConstraint(item: self, attribute: .bottom, relatedBy: .equal, toItem: container, attribute: .bottom, multiplier: 1.0, constant: 0.0)
        ])
    }
}
