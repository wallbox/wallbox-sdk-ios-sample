//
//  CredentialsHelper.swift
//  WallboxSDK-Sample
//
//  Created by Pia Muñoz on 25/1/21.
//

import Foundation

class CredentialsHelper {
    private static func readFile() -> String? {
        if let filepath = Bundle.main.path(forResource: "Credentials", ofType: "txt") {
            do {
                let contents = try String(contentsOfFile: filepath)
                return contents.components(separatedBy: "\"")[1]
            } catch {
                print("Credentials.txt failed")
                return nil
            }
        } else {
            print("Credentials.txt not found")
            return nil
        }
    }
    
    static var token: String? = readFile()
}
